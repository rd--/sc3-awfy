UnionFindNode  {
var parent_,bb_,dfsNumber_,loop;

initialize { dfsNumber_ = 0 }
initNodeDfs_ { arg bb,dfsNumber; parent_ = this ;bb_ = bb ;dfsNumber_ = dfsNumber }
loop { ^loop }
loop_ { arg aLoop; loop = aLoop }
findSet { var nodeList,node; nodeList = Vector .new ;node = this ;{node != node .parent} .whileTrue_({((node .parent) != (node .parent .parent)) .ifTrue_({nodeList .append_(node)}) ;node = node .parent}) ;nodeList .forEach_({arg iter; iter .union_(parent_)}) ;^node }
union_ { arg basicBlock; parent_ = basicBlock }
parent { ^parent_ }
bb { ^bb_ }
dfsNumber { ^dfsNumber_ }

* new { ^super .new .initialize }

}
