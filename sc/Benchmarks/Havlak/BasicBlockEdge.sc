BasicBlockEdge  {
var from,to;

initFromTo_ { arg cfg,fromName,toName; from = cfg .createNode_(fromName) ;to = cfg .createNode_(toName) ;from .addOutEdge_(to) ;to .addInEdge_(from) ;cfg .addEdge_(this) }

* forFromTo_ { arg cfg,fromName,toName; ^this .new .initFromTo_(cfg,fromName,toName) }

}
