SimpleLoop  {
var counter,depthLevel,parent_,isRoot_,nestingLevel_,header,isReducible,basicBlocks,children;

initReducible_ { arg aBB,aBool; counter = 0 ;depthLevel = 0 ;isRoot_ = false ;nestingLevel_ = 0 ;header = aBB ;isReducible = aBool ;basicBlocks = SomIdentitySet .new ;children = SomIdentitySet .new ;aBB .notNil .ifTrue_({basicBlocks .add_(aBB)}) }
counter { ^counter }
counter_ { arg val; counter = val }
depthLevel { ^depthLevel }
depthLevel_ { arg val; depthLevel = val }
children { ^children }
addNode_ { arg bb; basicBlocks .add_(bb) }
addChildLoop_ { arg loop; children .add_(loop) }
parent { ^parent_ }
parent_ { arg val; parent_ = val ;parent_ .addChildLoop_(this) }
isRoot { ^isRoot_ }
setIsRoot { isRoot_ = true }
nestingLevel { ^nestingLevel_ }
nestingLevel_ { arg level; nestingLevel_ = level ;( level == 0 ) .ifTrue_({this .setIsRoot}) }

* basicBlockReducible_ { arg bb,isReducible; ^this .new .initReducible_(bb,isReducible) }

}
