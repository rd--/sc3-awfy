HavlakLoopFinder  {
var cfg,lsg,nonBackPreds,backPreds,number,maxSize,header,type,last,nodes;

initLsg_ { arg aCfg,aLsg; cfg = aCfg ;lsg = aLsg ;nonBackPreds = Vector .new ;backPreds = Vector .new ;number = SomIdentityDictionary .new ;maxSize = 0 }
unvisited { ^2147483647 }
maxNonBackPreds { ^32 * 1024 }
isAncestorV_ { arg w,v; ^(w <= v) .and_({(v <= (last .at_(w)))}) }
doDFSCurrent_ { arg currentNode,current; var lastId,outerBlocks; (nodes .at_(current)) .initNodeDfs_(currentNode,current) ;number .atPut_(currentNode,current) ;lastId = current ;outerBlocks = currentNode .outEdges ;1 .toDo_(outerBlocks .size,{arg i; var target; target = outerBlocks .at_(i) ;( (number .at_(target)) == this .unvisited ) .ifTrue_({lastId = this .doDFSCurrent_(target,lastId + 1)})}) ;last .atPut_(current,lastId) ;^lastId }
initAllNodes { cfg .basicBlocks .forEach_({arg bb; number .atPut_(bb,this .unvisited)}) ;this .doDFSCurrent_(cfg .startBasicBlock,1) }
identifyEdges_ { arg size; 1 .toDo_(size,{arg w; var nodeW; header .atPut_(w,1) ;type .atPut_(w,\BBNonHeader) ;nodeW = (nodes .at_(w)) .bb ;nodeW .isNil .ifTrueIfFalse_({type .atPut_(w,\BBDead)},{this .processEdgesW_(nodeW,w)})}) }
processEdgesW_ { arg nodeW,w; ( nodeW .numPred > 0 ) .ifTrue_({nodeW .inEdges .forEach_({arg nodeV; var v; v = number .at_(nodeV) ;( v != this .unvisited ) .ifTrue_({(this .isAncestorV_(w,v)) .ifTrueIfFalse_({(backPreds .at_(w)) .append_(v)},{(nonBackPreds .at_(w)) .add_(v)})})})}) }
findLoops { var size; cfg .startBasicBlock .isNil .ifTrue_({^this}) ;size = cfg .numNodes ;nonBackPreds .removeAll ;backPreds .removeAll ;number .removeAll ;( size > maxSize ) .ifTrue_({header = Array .new_(size) ;type = Array .new_(size) ;last = Array .new_(size) ;nodes = Array .new_(size) ;maxSize = size}) ;1 .toDo_(size,{arg i; nonBackPreds .append_(SomSet .new) ;backPreds .append_(Vector .new) ;nodes .atPut_(i,UnionFindNode .new)}) ;this .initAllNodes ;this .identifyEdges_(size) ;header .atPut_(1,1) ;size .toByDo_(1,-1,{arg w; var nodePool,nodeW; nodePool = Vector .new ;nodeW = (nodes .at_(w)) .bb ;nodeW .notNil .ifTrue_({var workList; this .stepDNodePool_(w,nodePool) ;workList = Vector .new ;nodePool .forEach_({arg niter; workList .append_(niter)}) ;( nodePool .size != 0 ) .ifTrue_({type .atPut_(w,\BBReducible)}) ;{workList .isEmpty} .whileFalse_({var x,nonBackSize; x = workList .removeFirst ;nonBackSize = (nonBackPreds .at_(x .dfsNumber)) .size ;( nonBackSize > this .maxNonBackPreds ) .ifTrue_({^this}) ;this .stepEProcessNonBackPredsNodePoolWorkListX_(w,nodePool,workList,x)}) ;(( nodePool .size > 0 ) .or_({(type .at_(w)) == \BBSelf})) .ifTrue_({var loop; loop = lsg .createNewLoopReducible_(nodeW,((type .at_(w)) != \BBIrreducible)) ;this .setLoopAttributeNodePoolLoop_(w,nodePool,loop)})})}) }
stepEProcessNonBackPredsNodePoolWorkListX_ { arg w,nodePool,workList,x; (nonBackPreds .at_(x .dfsNumber)) .forEach_({arg iter; var y,ydash; y = nodes .at_(iter) ;ydash = y .findSet ;(this .isAncestorV_(w,ydash .dfsNumber)) .not .ifTrueIfFalse_({type .atPut_(w,\BBIrreducible) ;(nonBackPreds .at_(w)) .add_(ydash .dfsNumber)},{( ydash .dfsNumber != w ) .ifTrue_({(nodePool .hasSome_({arg e; e == ydash})) .ifFalse_({workList .append_(ydash) ;nodePool .append_(ydash)})})})}) }
setLoopAttributeNodePoolLoop_ { arg w,nodePool,loop; (nodes .at_(w)) .loop_(loop) ;nodePool .forEach_({arg node; header .atPut_(node .dfsNumber,w) ;node .union_((nodes .at_(w))) ;node .loop .notNil .ifTrueIfFalse_({node .loop .parent_(loop)},{loop .addNode_(node .bb)})}) }
stepDNodePool_ { arg w,nodePool; (backPreds .at_(w)) .forEach_({arg v; ( v != w ) .ifTrueIfFalse_({nodePool .append_((nodes .at_(v)) .findSet)},{type .atPut_(w,\BBSelf)})}) }

* newLsg_ { arg cfg,lsg; ^this .new .initLsg_(cfg,lsg) }

}
