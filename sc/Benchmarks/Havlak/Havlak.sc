Havlak  : Benchmark {


innerBenchmarkLoop_ { arg innerIterations; ^this .verifyResultIterations_((LoopTesterApp .new .mainLoopPPP_(innerIterations,50,10,10,5)),innerIterations) }
verifyResultIterations_ { arg result,innerIterations; ( innerIterations == 15000 ) .ifTrue_({^( (result .at_(1)) == 46602 ) .and_({(result .at_(2)) == 5213})}) ;( innerIterations == 1500 ) .ifTrue_({^( (result .at_(1)) == 6102 ) .and_({(result .at_(2)) == 5213})}) ;( innerIterations == 150 ) .ifTrue_({^( (result .at_(1)) == 2052 ) .and_({(result .at_(2)) == 5213})}) ;( innerIterations == 15 ) .ifTrue_({^( (result .at_(1)) == 1647 ) .and_({(result .at_(2)) == 5213})}) ;( innerIterations == 1 ) .ifTrue_({^( (result .at_(1)) == 1605 ) .and_({(result .at_(2)) == 5213})}) ;ScriptConsole .println_(("No verification result for" ++ innerIterations .asString ++ " found")) ;ScriptConsole .println_(("Result is " ++ (result .at_(1)) .asString ++ ", " ++ (result .at_(2)) .asString)) ;^false }


}
