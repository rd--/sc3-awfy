ControlFlowGraph  {
var basicBlockMap,startNode,edgeList;

initialize { basicBlockMap = Vector .new ;edgeList = Vector .new }
createNode_ { arg name; var node; (basicBlockMap .at_(name)) .notNil .ifTrueIfFalse_({node = basicBlockMap .at_(name)},{node = BasicBlock .new_(name) ;basicBlockMap .atPut_(name,node)}) ;( this .numNodes == 1 ) .ifTrue_({startNode = node}) ;^node }
addEdge_ { arg edge; edgeList .append_(edge) }
numNodes { ^basicBlockMap .size }
startBasicBlock { ^startNode }
basicBlocks { ^basicBlockMap }

* new { ^super .new .initialize }

}
