LoopTesterApp  {
var cfg,lsg;

initialize { cfg = ControlFlowGraph .new ;lsg = LoopStructureGraph .new ;cfg .createNode_(1) }
buildDiamond_ { arg start; var bb0; bb0 = start ;BasicBlockEdge .forFromTo_(cfg,bb0,bb0 + 1) ;BasicBlockEdge .forFromTo_(cfg,bb0,bb0 + 2) ;BasicBlockEdge .forFromTo_(cfg,bb0 + 1,bb0 + 3) ;BasicBlockEdge .forFromTo_(cfg,bb0 + 2,bb0 + 3) ;^bb0 + 3 }
buildConnectEnd_ { arg start,end; BasicBlockEdge .forFromTo_(cfg,start,end) }
buildStraightN_ { arg start,n; 0 .toDo_(n - 1,{arg i; this .buildConnectEnd_(start + i,start + i + 1)}) ;^start + n }
buildBaseLoop_ { arg from; var header,diamond1,d11,diamond2,footer; header = this .buildStraightN_(from,1) ;diamond1 = this .buildDiamond_(header) ;d11 = this .buildStraightN_(diamond1,1) ;diamond2 = this .buildDiamond_(d11) ;footer = this .buildStraightN_(diamond2,1) ;this .buildConnectEnd_(diamond2,d11) ;this .buildConnectEnd_(diamond1,header) ;this .buildConnectEnd_(footer,from) ;footer = this .buildStraightN_(footer,1) ;^footer }
mainLoopPPP_ { arg numDummyLoops,findLoopIterations,parLoop,pparLoops,ppparLoops; this .constructSimpleCFG ;this .addDummyLoops_(numDummyLoops) ;this .constructCFGPP_(parLoop,pparLoops,ppparLoops) ;this .findLoops_(lsg) ;findLoopIterations .timesRepeat_({this .findLoops_(LoopStructureGraph .new)}) ;lsg .calculateNestingLevel ; [\mainLoopPPP_,lsg .numLoops,cfg .numNodes].postln; ^Array .withWith_(lsg .numLoops,cfg .numNodes) }
constructCFGPP_ { arg parLoops,pparLoops,ppparLoops; var n; n = 3 ;parLoops .timesRepeat_({cfg .createNode_(n + 1) ;this .buildConnectEnd_(2,n + 1) ;n = n + 1 ;pparLoops .timesRepeat_({var top,bottom; top = n ;n = this .buildStraightN_(n,1) ;ppparLoops .timesRepeat_({n = this .buildBaseLoop_(n)}) ;bottom = this .buildStraightN_(n,1) ;this .buildConnectEnd_(n,top) ;n = bottom}) ;this .buildConnectEnd_(n,1)}) }
addDummyLoops_ { arg numDummyLoops; numDummyLoops .timesRepeat_({this .findLoops_(lsg)}) }
findLoops_ { arg loopStructure; var finder; finder = HavlakLoopFinder .newLsg_(cfg,loopStructure) ;finder .findLoops }
constructSimpleCFG { cfg .createNode_(1) ;this .buildBaseLoop_(1) ;cfg .createNode_(2) ;BasicBlockEdge .forFromTo_(cfg,1,3) }

* new { ^super .new .initialize }

}
