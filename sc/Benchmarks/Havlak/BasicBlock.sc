BasicBlock  {
var inEdges,outEdges,name;

init_ { arg aName; inEdges = Vector .new_(2) ;outEdges = Vector .new_(2) ;name = aName }
inEdges { ^inEdges }
outEdges { ^outEdges }
numPred { ^inEdges .size }
addOutEdge_ { arg to; outEdges .append_(to) }
addInEdge_ { arg from; inEdges .append_(from) }
customHash { ^name }

* new_ { arg name; ^this .new .init_(name) }

}
