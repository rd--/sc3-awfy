LoopStructureGraph  {
var root,loops,loopCounter;

initialize { root = SimpleLoop .basicBlockReducible_(nil,false) ;loops = Vector .new ;loopCounter = 0 ;root .nestingLevel_(0) ;root .counter_(loopCounter) ;loopCounter = loopCounter + 1 ;loops .append_(root) }
createNewLoopReducible_ { arg bb,isReducible; var loop; loop = SimpleLoop .basicBlockReducible_(bb,isReducible) ;loop .counter_(loopCounter) ;loopCounter = loopCounter + 1 ;loops .append_(loop) ;^loop }
calculateNestingLevel { loops .forEach_({arg liter; liter .isRoot .ifFalse_({liter .parent .isNil .ifTrue_({liter .parent_(root)})})}) ;this .calculateNestingLevelRecDepth_(root,0) }
calculateNestingLevelRecDepth_ { arg loop,depth; loop .depthLevel_(depth) ;loop .children .forEach_({arg liter; this .calculateNestingLevelRecDepth_(liter,depth + 1) ;loop .nestingLevel_((loop .nestingLevel .max_(1 + liter .nestingLevel)))}) }
numLoops { ^loops .size }

* new { ^super .new .initialize }

}
