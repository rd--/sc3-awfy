JsonLiteral  : JsonValue {
var value,isNull,isTrue,isFalse;
classvar nullLiteral,trueLiteral,falseLiteral;
initializeWith_ { arg val; value = val ;isNull = "null" == val ;isTrue = "true" == val ;isFalse = "false" == val }
asString { ^value }
isNull { ^isNull }
isTrue { ^isTrue }
isFalse { ^isFalse }
isBoolean { ^isTrue || isFalse }

* initialize { nullLiteral = this .new .initializeWith_("null") ;trueLiteral = this .new .initializeWith_("true") ;falseLiteral = this .new .initializeWith_("false") }
* nullLiteral { ^nullLiteral }
* trueLiteral { ^trueLiteral }
* falseLiteral { ^falseLiteral }

}
