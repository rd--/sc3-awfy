JsonArray  : JsonValue {
var values;

initialize { values = Vector .new }
add_ { arg value; value .ifNil_({this .error_("value is null")}) ;values .append_(value) }
size { ^values .size }
at_ { arg index; ^values .at_(index) }
isArray { ^true }
asArray { ^this }

* new { ^super .new .initialize }

}
