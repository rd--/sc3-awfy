ParseException  {
var offset,line,column,msg;

initializeWithAtLineColumn_ { arg message,anOffset,aLine,aColumn; msg = message ;offset = anOffset ;line = aLine ;column = aColumn }
message { ^msg }
offset { ^offset }
line { ^line }
column { ^column }
asString { ^msg + ":" ++ line ++ ":" ++ column }

* withAtLineColumn_ { arg aMessageString,offset,line,column; ^this .new .initializeWithAtLineColumn_(aMessageString,offset,line,column) }

}
