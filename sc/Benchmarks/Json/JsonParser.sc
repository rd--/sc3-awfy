JsonParser  {
var input,index,line,column,current,captureBuffer,captureStart,exceptionBlock;

initializeWith_ { arg string; input = string ;index = 0 ;line = 1 ;column = 0 ;current = nil ;captureBuffer = "" ;captureStart = -1 }
parse { var result; exceptionBlock = {arg ex; ^ex} ;this .read ;this .skipWhiteSpace ;result = this .readValue ;this .skipWhiteSpace ;this .isEndOfText .ifFalse_({this .error_("Unexpected character")}) ;^result }
readValue { ( current == "n" ) .ifTrue_({^this .readNull}) ;( current == "t" ) .ifTrue_({^this .readTrue}) ;( current == "f" ) .ifTrue_({^this .readFalse}) ;( current == "\"" ) .ifTrue_({^this .readString}) ;( current == "[" ) .ifTrue_({^this .readArray}) ;( current == "{" ) .ifTrue_({^this .readObject}) ;( current == "-" ) .ifTrue_({^this .readNumber}) ;( current == "0" ) .ifTrue_({^this .readNumber}) ;( current == "1" ) .ifTrue_({^this .readNumber}) ;( current == "2" ) .ifTrue_({^this .readNumber}) ;( current == "3" ) .ifTrue_({^this .readNumber}) ;( current == "4" ) .ifTrue_({^this .readNumber}) ;( current == "5" ) .ifTrue_({^this .readNumber}) ;( current == "6" ) .ifTrue_({^this .readNumber}) ;( current == "7" ) .ifTrue_({^this .readNumber}) ;( current == "8" ) .ifTrue_({^this .readNumber}) ;( current == "9" ) .ifTrue_({^this .readNumber}) ;this .expected_("value") }
readArrayElement_ { arg array; this .skipWhiteSpace ;array .add_(this .readValue) ;this .skipWhiteSpace }
readArray { var array; this .read ;array = JsonArray .new ;this .skipWhiteSpace ;(this .readChar_("]")) .ifTrue_({^array}) ;this .readArrayElement_(array) ;{this .readChar_(",")} .whileTrue_({this .readArrayElement_(array)}) ;(this .readChar_("]")) .ifFalse_({this .expected_(", or ]")}) ;^array }
readObjectKeyValuePair_ { arg object; var name; this .skipWhiteSpace ;name = this .readName ;this .skipWhiteSpace ;(this .readChar_(":")) .ifFalse_({this .expected_(":")}) ;this .skipWhiteSpace ;object .addWith_(name,this .readValue) ;this .skipWhiteSpace }
readObject { var object; this .read ;object = JsonObject .new ;this .skipWhiteSpace ;(this .readChar_("}")) .ifTrue_({^object}) ;this .readObjectKeyValuePair_(object) ;{this .readChar_(",")} .whileTrue_({this .readObjectKeyValuePair_(object)}) ;(this .readChar_("}")) .ifFalse_({this .expected_(", or }")}) ;^object }
readName { ( current == "\"" ) .ifFalse_({this .expected_("name")}) ;^this .readStringInternal }
readNull { this .read ;this .readRequiredChar_("u") ;this .readRequiredChar_("l") ;this .readRequiredChar_("l") ;^JsonLiteral .nullLiteral }
readTrue { this .read ;this .readRequiredChar_("r") ;this .readRequiredChar_("u") ;this .readRequiredChar_("e") ;^JsonLiteral .trueLiteral }
readFalse { this .read ;this .readRequiredChar_("a") ;this .readRequiredChar_("l") ;this .readRequiredChar_("s") ;this .readRequiredChar_("e") ;^JsonLiteral .falseLiteral }
readRequiredChar_ { arg ch; (this .readChar_(ch)) .ifFalse_({this .expected_("character: " ++ ch)}) }
readString { ^JsonString .new_(this .readStringInternal) }
readStringInternal { var string; this .read ;this .startCapture ;{current == "\""} .whileFalse_({( current == "\\" ) .ifTrueIfFalse_({this .pauseCapture ;this .readEscape ;this .startCapture},{this .read})}) ;string = this .endCapture ;this .read ;^string }
readEscapeChar { ( current == "\"" ) .ifTrue_({^"\""}) ;( current == "/" ) .ifTrue_({^"/"}) ;( current == "\\" ) .ifTrue_({^"\\"}) ;( current == "b" ) .ifTrue_({^"\b"}) ;( current == "f" ) .ifTrue_({^"\f"}) ;( current == "n" ) .ifTrue_({^"\n"}) ;( current == "r" ) .ifTrue_({^"\r"}) ;( current == "t" ) .ifTrue_({^"\t"}) ;this .expected_("valid escape sequence. note, some are not supported") }
readEscape { this .read ;captureBuffer = captureBuffer .concatenate_(this .readEscapeChar) ;this .read }
readNumber { var firstDigit; this .startCapture ;this .readChar_("-") ;firstDigit = current ;this .readDigit .ifFalse_({this .expected_("digit")}) ;( firstDigit != "0" ) .ifTrue_({{this .readDigit} .whileTrue_({})}) ;this .readFraction ;this .readExponent ;^JsonNumber .new_(this .endCapture) }
readFraction { (this .readChar_(".")) .ifFalse_({^false}) ;this .readDigit .ifFalse_({this .expected_("digit")}) ;{this .readDigit} .whileTrue_({}) ;^true }
readExponent { ((this .readChar_("e")) .not .and_({(this .readChar_("E")) .not})) .ifTrue_({^false}) ;(this .readChar_("+")) .ifFalse_({this .readChar_("-")}) ;this .readDigit .ifFalse_({this .expected_("digit")}) ;{this .readDigit} .whileTrue_({}) ;^true }
readChar_ { arg ch; ( current == ch ) .ifFalse_({^false}) ;this .read ;^true }
readDigit { this .isDigit .ifFalse_({^false}) ;this .read ;^true }
skipWhiteSpace { {this .isWhiteSpace} .whileTrue_({this .read}) }
read { ( current == "\n" ) .ifTrue_({line = line + 1 ;column = 0}) ;index = index + 1 ;column = column + 1 ;input .ifNil_({this .error_("input nil")}) ;( index <= input .size ) .ifTrueIfFalse_({current = (input .stringAt_(index)) .asString},{current = nil}) }
startCapture { captureStart = index }
pauseCapture { captureBuffer = captureBuffer .concatenate_((input .copyFromTo_(captureStart,index - 1))) ;captureStart = -1 }
endCapture { var captured; ( "" == captureBuffer ) .ifTrueIfFalse_({captured = input .copyFromTo_(captureStart,index - 1)},{this .pauseCapture ;captured = captureBuffer ;captureBuffer = ""}) ;captureStart = -1 ;^captured }
expected_ { arg expected; this .isEndOfText .ifTrue_({this .error_("Unexpected end of input, expected " ++ expected .asString)}) ;this .error_("Expected " ++ expected) }
error_ { arg message; exceptionBlock .value_((ParseException .withAtLineColumn_(message,index,line,column))) }
isWhiteSpace { ( current == " " ) .ifTrue_({^true}) ;( current == "\t" ) .ifTrue_({^true}) ;( current == "\n" ) .ifTrue_({^true}) ;( current == "\r" ) .ifTrue_({^true}) ;^false }
isDigit { ( current == "0" ) .ifTrue_({^true}) ;( current == "1" ) .ifTrue_({^true}) ;( current == "2" ) .ifTrue_({^true}) ;( current == "3" ) .ifTrue_({^true}) ;( current == "4" ) .ifTrue_({^true}) ;( current == "5" ) .ifTrue_({^true}) ;( current == "6" ) .ifTrue_({^true}) ;( current == "7" ) .ifTrue_({^true}) ;( current == "8" ) .ifTrue_({^true}) ;( current == "9" ) .ifTrue_({^true}) ;^false }
isEndOfText { ^current .isNil }

* with_ { arg aJsonString; ^this .new .initializeWith_(aJsonString) }

}
