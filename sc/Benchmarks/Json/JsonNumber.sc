JsonNumber  : JsonValue {
var string;

initializeWith_ { arg str; string = str }
asString { ^string }
isNumber { ^true }

* new_ { arg string; string .ifNil_({this .error_("string is null")}) ;^this .new .initializeWith_(string) }

}
