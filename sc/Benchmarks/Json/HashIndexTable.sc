HashIndexTable  {
var hashTable;

initialize { hashTable = Array .newWithAll_(32,0) }
atPut_ { arg name,index; var slot; slot = this .hashSlotFor_(name) ;( index < 255 ) .ifTrueIfFalse_({hashTable .atPut_(slot,index + 1)},{hashTable .atPut_(slot,0)}) }
at_ { arg name; var slot; slot = this .hashSlotFor_(name) ;^((hashTable .at_(slot)) & 255) - 1 }
stringHash_ { arg s; ^s .size * 1402589 }
hashSlotFor_ { arg element; ^((this .stringHash_(element)) & (hashTable .size - 1)) + 1 }

* new { ^super .new .initialize }

}
