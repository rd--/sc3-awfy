JsonValue  {


isObject { ^false }
isArray { ^false }
isNumber { ^false }
isString { ^false }
isBoolean { ^false }
isTrue { ^false }
isFalse { ^false }
isNull { ^false }
asObject { this .error_("Unsupported operation, not an object: " ++ this .asString) }
asArray { this .error_("Unsupported operation, not an array: " ++ this .asString) }


}
