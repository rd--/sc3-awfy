JsonString  : JsonValue {
var string;

initializeWith_ { arg str; string = str }
isString { ^true }

* new_ { arg str; ^this .new .initializeWith_(str) }

}
