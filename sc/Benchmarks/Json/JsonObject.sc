JsonObject  : JsonValue {
var names,values,table;

initialize { names = Vector .new ;values = Vector .new ;table = HashIndexTable .new }
addWith_ { arg name,aJsonValue; name .ifNil_({this .error_("name is null")}) ;aJsonValue .ifNil_({this .error_("aJsonValue is null")}) ;table .atPut_(name,names .size + 1) ;names .append_(name) ;values .append_(aJsonValue) }
at_ { arg name; var idx; name .ifNil_({this .error_("name is null")}) ;idx = this .indexOf_(name) ;( idx == 0 ) .ifTrueIfFalse_({^nil},{^values .at_(idx)}) }
size { ^names .size }
isEmpty { ^names .isEmpty }
isObject { ^true }
asObject { ^this }
indexOf_ { arg name; var idx; idx = table .at_(name) ;(( idx != 0 ) .and_({name == (names .at_(idx))})) .ifTrue_({^idx}) ;^this .error_("not implement") }

* new { ^super .new .initialize }
* readFrom_ { arg string; ^(JsonValue .readFrom_(string)) .asObject }

}
