Towers  : Benchmark {
var piles,movesdone;

initialize { piles = nil ;movesdone = 0 }
pushDiskOnPile_ { arg disk,pile; var top; top = piles .at_(pile) ;( top .notNil && {disk .size >= top .size} ) .ifTrue_({this .error_("Cannot put a big disk on a smaller one")}) ;disk .next_(top) ;piles .atPut_(pile,disk) }
popDiskFrom_ { arg pile; var top; top = piles .at_(pile) ;top .isNil .ifTrue_({this .error_("Attempting to remove a disk from an empty pile")}) ;piles .atPut_(pile,top .next) ;top .next_(nil) ;^top }
moveTopDiskFromTo_ { arg fromPile,toPile; this .pushDiskOnPile_((this .popDiskFrom_(fromPile)),toPile) ;movesdone = movesdone + 1 }
buildTowerAtDisks_ { arg pile,disks; disks .downToDo_(0,{arg i; this .pushDiskOnPile_((TowersDisk .new_(i)),pile)}) }
moveDisksFromTo_ { arg disks,fromPile,toPile; ( disks == 1 ) .ifTrueIfFalse_({this .moveTopDiskFromTo_(fromPile,toPile)},{var otherPile; otherPile = 6 - fromPile - toPile ;this .moveDisksFromTo_(disks - 1,fromPile,otherPile) ;this .moveTopDiskFromTo_(fromPile,toPile) ;this .moveDisksFromTo_(disks - 1,otherPile,toPile)}) }
benchmark { piles = Array .new_(3) ;this .buildTowerAtDisks_(1,13) ;movesdone = 0 ;this .moveDisksFromTo_(13,1,2) ;^movesdone }
verifyResult_ { arg result; ^8191 == result }

* new { ^super .new .initialize }

}
