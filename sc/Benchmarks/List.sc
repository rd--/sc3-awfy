ListBenchmark  : Benchmark {


benchmark { var result; result = this .talkWithXWithYWithZ_((this .makeList_(15)),(this .makeList_(10)),(this .makeList_(6))) ;^result .length }
verifyResult_ { arg result; ^10 == result }
makeList_ { arg length; (length == 0) .ifTrueIfFalse_({^nil},{var e; e = ListElement .new_(length) ;e .next_((this .makeList_((length - 1)))) ;^e}) }
isShorterThan_ { arg x,y; var xTail,yTail; xTail = x ;yTail = y ;{yTail .isNil} .whileFalse_({xTail .isNil .ifTrue_({^true}) ;xTail = xTail .next ;yTail = yTail .next}) ;^false }
talkWithXWithYWithZ_ { arg x,y,z; (this .isShorterThan_(y,x)) .ifTrueIfFalse_({^(this .talkWithXWithYWithZ_((this .talkWithXWithYWithZ_(x .next,y,z)),(this .talkWithXWithYWithZ_(y .next,z,x)),(this .talkWithXWithYWithZ_(z .next,x,y))))},{^z}) }


}
