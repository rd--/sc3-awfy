Bounce  : Benchmark {


benchmark { var ballCount,balls,bounces,random; random = SomRandom .new ;ballCount = 100 ;bounces = 0 ;balls = Array .newWithAll_(ballCount,{BounceBall .new_(random)}) ;1 .toDo_(50,{arg i; balls .do_({arg ball; (ball .bounce) .ifTrue_({bounces = bounces + 1})})}) ;^bounces }
verifyResult_ { arg result; ^1331 == result }


}
