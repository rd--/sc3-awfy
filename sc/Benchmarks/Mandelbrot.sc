Mandelbrot  : Benchmark {


innerBenchmarkLoop_ { arg innerIterations; ^this .verifyInner_((this .mandelbrot_(innerIterations)),innerIterations) }
verifyInner_ { arg result,innerIterations; ( innerIterations == 500 ) .ifTrue_({^result == 191}) ;( innerIterations == 750 ) .ifTrue_({^result == 50}) ;( innerIterations == 1 ) .ifTrue_({^result == 128}) ;ScriptConsole .println_(("No verification result for " ++ innerIterations .asString ++ " found")) ;ScriptConsole .println_(("Result is: " ++ result .asString)) ;^false }
mandelbrot_ { arg size; var sum,byteAcc,bitNum,y; sum = 0 ;byteAcc = 0 ;bitNum = 0 ;y = 0 ;{y < size} .whileTrue_({var ci,x; ci = (2.0 * y / size) - 1.0 ;x = 0 ;{x < size} .whileTrue_({var zr,zrzr,zi,zizi,cr,escape,z,notDone; zrzr = zr = 0.0 ;zizi = zi = 0.0 ;cr = (2.0 * x / size) - 1.5 ;z = 0 ;notDone = true ;escape = 0 ;{notDone .and_({z < 50})} .whileTrue_({zr = zrzr - zizi + cr ;zi = 2.0 * zr * zi + ci ;zrzr = zr * zr ;zizi = zi * zi ;(zrzr + zizi > 4.0) .ifTrue_({notDone = false ;escape = 1}) ;z = z + 1}) ;byteAcc = (byteAcc << 1) + escape ;bitNum = bitNum + 1 ;( bitNum == 8 ) .ifTrueIfFalse_({sum = sum .bitXor_(byteAcc) ;byteAcc = 0 ;bitNum = 0},{(x == (size - 1)) .ifTrue_({byteAcc = byteAcc << (8 - bitNum) ;sum = sum .bitXor_(byteAcc) ;byteAcc = 0 ;bitNum = 0})}) ;x = x + 1}) ;y = y + 1}) ;^sum }


}
