BounceBall  : Object {
var x,y,xVel,yVel;

bounce { var xLimit,yLimit,bounced; xLimit = yLimit = 500 ;bounced = false ;x = x + xVel ;y = y + yVel ;( x > xLimit ) .ifTrue_({x = xLimit ;xVel = 0 - xVel .abs ;bounced = true}) ;( x < 0 ) .ifTrue_({x = 0 ;xVel = xVel .abs ;bounced = true}) ;( y > yLimit ) .ifTrue_({y = yLimit ;yVel = 0 - yVel .abs ;bounced = true}) ;( y < 0 ) .ifTrue_({y = 0 ;yVel = yVel .abs ;bounced = true}) ;^bounced }
initialize_ { arg random; x = random .next % 500 ;y = random .next % 500 ;xVel = (random .next % 300) - 150 ;yVel = (random .next % 300) - 150 }

* new_ { arg random; ^this .new .initialize_(random) }

}
