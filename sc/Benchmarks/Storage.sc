Storage  : Benchmark {
var count;

initialize { count = 0 }
benchmark { var random; random = SomRandom .new ;count = 0 ;this .buildTreeDepthWith_(7,random) ;^count }
verifyResult_ { arg result; ^5461 == result }
buildTreeDepthWith_ { arg depth,random; count = count + 1 ;^(depth == 1) .ifTrueIfFalse_({Array .new_(random .next % 10 + 1)},{Array .newWithAll_(4,{this .buildTreeDepthWith_(depth - 1,random)})}) }

* new { ^super .new .initialize }

}
