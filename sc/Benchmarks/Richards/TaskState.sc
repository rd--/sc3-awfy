TaskState  : RBObject {
var packetPending,taskWaiting,taskHolding;

isPacketPending { ^packetPending }
isTaskHolding { ^taskHolding }
isTaskWaiting { ^taskWaiting }
taskHolding_ { arg aBoolean; taskHolding = aBoolean }
taskWaiting_ { arg aBoolean; taskWaiting = aBoolean }
packetPending_ { arg aBoolean; packetPending = aBoolean }
packetPending { packetPending = true ;taskWaiting = false ;taskHolding = false }
running { packetPending = taskWaiting = taskHolding = false }
waiting { packetPending = taskHolding = false ;taskWaiting = true }
waitingWithPacket { taskHolding = false ;taskWaiting = packetPending = true }
isRunning { ^packetPending .not .and_({taskWaiting .not .and_({taskHolding .not})}) }
isTaskHoldingOrWaiting { ^taskHolding .or_({packetPending .not .and_({taskWaiting})}) }
isWaiting { ^packetPending .not .and_({taskWaiting .and_({taskHolding .not})}) }
isWaitingWithPacket { ^packetPending .and_({taskWaiting .and_({taskHolding .not})}) }

* packetPending { ^super .new .packetPending }
* running { ^super .new .running }
* waiting { ^super .new .waiting }
* waitingWithPacket { ^super .new .waitingWithPacket }

}
