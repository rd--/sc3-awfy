RBObject  : Object {


appendHead_ { arg packet,queueHead; var mouse,link; packet .link_(RBObject .noWork) ;( RBObject .noWork == queueHead ) .ifTrue_({^packet}) ;mouse = queueHead ;{RBObject .noWork == (link = mouse .link)} .whileFalse_({mouse = link}) ;mouse .link_(packet) ;^queueHead }

* noTask { ^nil }
* idler { ^1 }
* noWork { ^nil }
* worker { ^2 }
* workPacketKind { ^2 }
* handlerA { ^3 }
* handlerB { ^4 }
* deviceA { ^5 }
* deviceB { ^6 }
* devicePacketKind { ^1 }

}
