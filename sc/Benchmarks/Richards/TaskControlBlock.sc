TaskControlBlock  : TaskState {
var link,identity,priority,input,function,handle;

identity { ^identity }
link { ^link }
priority { ^priority }
linkIdentityPriorityInitialWorkQueueInitialStateFunctionPrivateData_ { arg aLink,anIdentity,aPriority,anInitialWorkQueue,anInitialState,aBlock,aPrivateData; link = aLink ;identity = anIdentity ;function = aBlock ;priority = aPriority ;input = anInitialWorkQueue ;handle = aPrivateData ;this .packetPending_(anInitialState .isPacketPending) ;this .taskWaiting_(anInitialState .isTaskWaiting) ;this .taskHolding_(anInitialState .isTaskHolding) }
addInputCheckPriority_ { arg packet,oldTask; ( RBObject .noWork == input ) .ifTrueIfFalse_({input = packet ;this .packetPending_(true) ;( priority > oldTask .priority ) .ifTrue_({^this})},{input = this .appendHead_(packet,input)}) ;^oldTask }
runTask { var message; this .isWaitingWithPacket .ifTrueIfFalse_({message = input ;input = message .link ;( RBObject .noWork == input ) .ifTrueIfFalse_({this .running},{this .packetPending})},{message = RBObject .noWork}) ;^function .valueValue_(message,handle) }

* linkCreatePriorityInitialWorkQueueInitialStateFunctionPrivateData_ { arg link,identity,priority,initialWorkQueue,initialState,aBlock,privateData; ^super .new .linkIdentityPriorityInitialWorkQueueInitialStateFunctionPrivateData_(link,identity,priority,initialWorkQueue,initialState,aBlock,privateData) }

}
