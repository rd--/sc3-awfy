Packet  : RBObject {
var link,identity,kind,datum,data;

data { ^data }
datum { ^datum }
datum_ { arg someData; datum = someData }
identity { ^identity }
identity_ { arg anIdentity; identity = anIdentity }
kind { ^kind }
link { ^link }
link_ { arg aWorkQueue; link = aWorkQueue }
linkIdentityKind_ { arg aLink,anIdentity,aKind; link = aLink ;kind = aKind ;identity = anIdentity ;datum = 1 ;data = Array .newWithAll_(4,0) }
asString { ^"Packet(" ++ link .asString ++ ", " ++ identity .asString ++ ", " ++ kind .asString ++ ", " ++ datum .asString ++ ", " ++ data .asString ++ ")" }

* createIdentityKind_ { arg link,identity,kind; ^super .new .linkIdentityKind_(link,identity,kind) }

}
