HandlerTaskDataRecord  : RBObject {
var workIn,deviceIn;

deviceIn { ^deviceIn }
deviceIn_ { arg aPacket; deviceIn = aPacket }
deviceInAdd_ { arg packet; deviceIn = this .appendHead_(packet,deviceIn) }
workIn { ^workIn }
workIn_ { arg aWorkQueue; workIn = aWorkQueue }
workInAdd_ { arg packet; workIn = this .appendHead_(packet,workIn) }
create { workIn = deviceIn = RBObject .noWork }
asString { ^"HandlerTaskDataRecord(" ++ workIn .asString ++ ", " ++ deviceIn .asString ++ ")" }

* create { ^super .new .create }

}
