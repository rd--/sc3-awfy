DeviceTaskDataRecord  : RBObject {
var pending;

pending { ^pending }
pending_ { arg packet; pending = packet }
create { pending = RBObject .noWork }

* create { ^super .new .create }

}
