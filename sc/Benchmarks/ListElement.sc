ListElement  {
var val,next;

length { next .isNil .ifTrueIfFalse_({^1},{^(1 + next .length)}) }
val { ^val }
val_ { arg n; val = n }
next { ^next }
next_ { arg element; next = element }
initialize_ { arg n; val = n ;next = nil }

* new_ { arg n; ^this .new .initialize_(n) }

}
