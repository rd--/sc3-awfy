DictEntry  {
var hash,key,value,next;

initKeyValueNext_ { arg aHash,aKey,val,anEntry; hash = aHash ;key = aKey ;value = val ;next = anEntry }
hash { ^hash }
key { ^key }
value { ^value }
value_ { arg val; value = val }
next { ^next }
next_ { arg e; next = e }
matchKey_ { arg aHash,aKey; ^( hash == aHash ) .and_({key == aKey}) }

* newKeyValueNext_ { arg hash,key,val,next; ^this .new .initKeyValueNext_(hash,key,val,next) }

}
