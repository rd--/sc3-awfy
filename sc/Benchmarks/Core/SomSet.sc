SomSet  {
var items;

initialize_ { arg size; items = Vector .new_(size) }
forEach_ { arg block; items .forEach_(block) }
hasSome_ { arg block; ^items .hasSome_(block) }
getOne_ { arg block; ^items .getOne_(block) }
add_ { arg anObject; (this .contains_(anObject)) .ifFalse_({items .append_(anObject)}) }
collect_ { arg block; var coll; coll = Vector .new ;this .forEach_({arg e; coll .append_((block .value_(e)))}) ;^coll }
contains_ { arg anObject; ^this .hasSome_({arg it; it == anObject}) }
size { ^items .size }
removeAll { ^items .removeAll }

* new { ^super .new .initialize_(10) }

}
