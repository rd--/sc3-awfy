DictIdEntry  : DictEntry {


matchKey_ { arg aHash,aKey; ^( hash == aHash ) .and_({key == aKey}) }

* newKeyValueNext_ { arg hash,key,val,next; ^this .new .initKeyValueNext_(hash,key,val,next) }

}
