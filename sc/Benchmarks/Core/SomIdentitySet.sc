SomIdentitySet  : SomSet {


contains_ { arg anObject; ^this .hasSome_({arg it; it == anObject}) }

* new_ { arg size; ^super .new .initialize_(size) }

}
