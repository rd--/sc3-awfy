SomDictionary  {
var buckets,size_;

initialize_ { arg size; buckets = Array .new_(size) ;size_ = 0 }
hash_ { arg key; var hash; key .isNil .ifTrue_({^0}) ;hash = key .customHash ;^hash .bitXor_((hash >> 16)) }
bucketIdx_ { arg hash; ^1 + ((buckets .size - 1) & hash) }
bucket_ { arg hash; ^buckets .at_((this .bucketIdx_(hash))) }
at_ { arg aKey; var hash,e; hash = this .hash_(aKey) ;e = this .bucket_(hash) ;{e .notNil} .whileTrue_({(e .matchKey_(hash,aKey)) .ifTrue_({^e .value}) ;e = e .next}) ;^nil }
containsKey_ { arg aKey; var hash,e; hash = this .hash_(aKey) ;e = this .bucket_(hash) ;{e .notNil} .whileTrue_({(e .matchKey_(hash,aKey)) .ifTrue_({^true}) ;e = e .next}) ;^false }
atPut_ { arg aKey,aVal; var hash,i,current; hash = this .hash_(aKey) ;i = this .bucketIdx_(hash) ;current = buckets .at_(i) ;current .isNil .ifTrueIfFalse_({buckets .atPut_(i,(this .newEntryValueHash_(aKey,aVal,hash))) ;size_ = size_ + 1},{this .insertBucketEntryValueHashHead_(aKey,aVal,hash,current)}) ;( size_ > buckets .size ) .ifTrue_({this .resize}) }
newEntryValueHash_ { arg aKey,value,hash; ^DictEntry .newKeyValueNext_(hash,aKey,value,nil) }
insertBucketEntryValueHashHead_ { arg key,value,hash,head; var current; current = head ;{true} .whileTrue_({(current .matchKey_(hash,key)) .ifTrue_({current .value_(value) ;^this}) ;current .next .isNil .ifTrue_({size_ = size_ + 1 ;current .next_((this .newEntryValueHash_(key,value,hash))) ;^this}) ;current = current .next}) }
resize { var oldStorage; oldStorage = buckets ;buckets = Array .new_(oldStorage .size * 2) ;this .transferEntries_(oldStorage) }
transferEntries_ { arg oldStorage; 1 .toDo_(oldStorage .size,{arg i; var current; current = oldStorage .at_(i) ;current .notNil .ifTrue_({oldStorage .atPut_(i,nil) ;current .next .isNil .ifTrueIfFalse_({buckets .atPut_(1 + (current .hash & (buckets .size - 1)),current)},{this .splitBucketBucketHead_(oldStorage,i,current)})})}) }
splitBucketBucketHead_ { arg oldStorage,i,head; var loHead,loTail,hiHead,hiTail,current; loHead = nil ;loTail = nil ;hiHead = nil ;hiTail = nil ;current = head ;{current .notNil} .whileTrue_({( (current .hash & oldStorage .size) == 0 ) .ifTrueIfFalse_({loTail .isNil .ifTrueIfFalse_({loHead = current},{loTail .next_(current)}) ;loTail = current},{hiTail .isNil .ifTrueIfFalse_({hiHead = current},{hiTail .next_(current)}) ;hiTail = current}) ;current = current .next}) ;loTail .notNil .ifTrue_({loTail .next_(nil) ;buckets .atPut_(i,loHead)}) ;hiTail .notNil .ifTrue_({hiTail .next_(nil) ;buckets .atPut_(i + oldStorage .size,hiHead)}) }
size { ^size_ }
isEmpty { ^size_ == 0 }
removeAll { buckets = Array .new_(buckets .size) ;size_ = 0 }
keys { var keys; keys = Vector .new_(size_) ;buckets .do_({arg b; var current; current = b ;{current .notNil} .whileTrue_({keys .append_(current .key) ;current = current .next})}) ;^keys }
values { var values; values = Vector .new_(size_) ;buckets .do_({arg b; var current; current = b ;{current .notNil} .whileTrue_({values .append_(current .value) ;current = current .next})}) ;^values }

* new_ { arg size; ^super .new .initialize_(size) }
* new { ^this .new_(16) }

}
