Vector  : Object {
var first,last,storage;

initialize_ { arg size; first = 1 ;last = 1 ;storage = Array .new_(size) }
at_ { arg index; ( index > storage .length ) .ifTrue_({^nil}) ;^storage .at_(index) }
atPut_ { arg index,val; ( index > storage .length ) .ifTrue_({var newLength,newStorage; newLength = storage .length ;{newLength < index} .whileTrue_({newLength = newLength * 2}) ;newStorage = Array .new_(newLength) ;storage .doIndexes_({arg i; newStorage .atPut_(i,(storage .at_(i)))}) ;storage = newStorage}) ;storage .atPut_(index,val) ;( last < (index + 1) ) .ifTrue_({last = index + 1}) }
append_ { arg element; (last > storage .length) .ifTrue_({var newStorage; newStorage = Array .new_((2 * storage .length)) ;storage .doIndexes_({arg i; newStorage .atPut_(i,(storage .at_(i)))}) ;storage = newStorage}) ;storage .atPut_(last,element) ;last = last + 1 ;^this }
isEmpty { ^last == first }
forEach_ { arg block; first .toDo_(last - 1,{arg i; block .value_((storage .at_(i)))}) }
hasSome_ { arg block; first .toDo_(last - 1,{arg i; (block .value_((storage .at_(i)))) .ifTrue_({^true})}) ;^false }
getOne_ { arg block; first .toDo_(last - 1,{arg i; var e; e = storage .at_(i) ;(block .value_(e)) .ifTrue_({^e})}) ;^nil }
removeFirst { this .isEmpty .ifTrue_({^nil}) ;first = first + 1 ;^storage .at_(first - 1) }
removeAll { first = 1 ;last = 1 ;storage = Array .new_(storage .length) }
remove_ { arg object; var newArray,newLast,found; newArray = Array .new_(this .capacity) ;newLast = 1 ;found = false ;this .forEach_({arg it; ( it == object ) .ifTrueIfFalse_({found = true},{newArray .atPut_(newLast,it) ;newLast = newLast + 1})}) ;storage = newArray ;last = newLast ;first = 1 ;^found }
size { ^last - first }
capacity { ^storage .length }
sort_ { arg aBlock; ( this .size > 0 ) .ifTrue_({this .sortToWith_(first,last - 1,aBlock)}) }
sortToWith_ { arg i,j,sortBlock; var di,dij,dj,tt,ij,k,l,n; sortBlock .isNil .ifTrue_({^this .defaultSortTo_(i,j)}) ;( (n = j + 1 - i) <= 1 ) .ifTrue_({^this}) ;di = storage .at_(i) ;dj = storage .at_(j) ;(sortBlock .valueWith_(di,dj)) .ifFalse_({storage .swapWith_(i,j) ;tt = di ;di = dj ;dj = tt}) ;( n > 2 ) .ifTrue_({ij = (i + j) / 2 ;dij = storage .at_(ij) ;(sortBlock .valueWith_(di,dij)) .ifTrueIfFalse_({(sortBlock .valueWith_(dij,dj)) .ifFalse_({storage .swapWith_(j,ij) ;dij = dj})},{storage .swapWith_(i,ij) ;dij = di}) ;( n > 3 ) .ifTrue_({k = i ;l = j ;{{l = l - 1 ;( k <= l ) .and_({sortBlock .valueWith_(dij,(storage .at_(l)))})} .whileTrue ;{k = k + 1 ;( k <= l ) .and_({sortBlock .valueWith_((storage .at_(k)),dij)})} .whileTrue ;k <= l} .whileTrue_({storage .swapWith_(k,l)}) ;this .sortToWith_(i,l,sortBlock) ;this .sortToWith_(k,j,sortBlock)})}) }

* new { ^this .new_(50) }
* new_ { arg initialSize; ^super .new .initialize_(initialSize) }
* with_ { arg elem; var newVector; newVector = this .new_(1) ;newVector .append_(elem) ;^newVector }

}
