Permute  : Benchmark {
var count,v;

benchmark { count = 0 ;v = Array .newWithAll_(6,0) ;this .permute_(6) ;^count }
verifyResult_ { arg result; ^8660 == result }
permute_ { arg n; count = count + 1 ;(n <> 0) .ifTrue_({this .permute_(n - 1) ;n .downToDo_(1,{arg i; this .swapWith_(n,i) ;this .permute_(n - 1) ;this .swapWith_(n,i)})}) }
swapWith_ { arg i,j; var tmp; tmp = v .at_(i) ;v .atPut_(i,(v .at_(j))) ;v .atPut_(j,tmp) }


}
