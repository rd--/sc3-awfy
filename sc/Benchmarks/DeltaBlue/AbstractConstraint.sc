AbstractConstraint  {
var strength;

initialize_ { arg strengthSymbol; strength = Strength .of_(strengthSymbol) }
strength { ^strength }
isInput { ^false }
isSatisfied { this .subclassResponsibility }
addConstraint_ { arg planner; this .addToGraph ;planner .incrementalAdd_(this) }
addToGraph { this .subclassResponsibility }
destroyConstraint_ { arg planner; this .isSatisfied .ifTrue_({planner .incrementalRemove_(this)}) ;this .removeFromGraph }
removeFromGraph { this .subclassResponsibility }
chooseMethod_ { arg mark; this .subclassResponsibility }
execute { this .subclassResponsibility }
inputsDo_ { arg aBlock; this .subclassResponsibility }
inputsKnown_ { arg mark; ^(this .inputsHasOne_({arg v; (( v .mark == mark ) .or_({v .stay .or_({v .determinedBy == nil})})) .not})) .not }
markUnsatisfied { this .subclassResponsibility }
output { this .subclassResponsibility }
recalculate { this .subclassResponsibility }
satisfyPropagate_ { arg mark,planner; var overridden; this .chooseMethod_(mark) ;this .isSatisfied .ifTrueIfFalse_({var out; this .inputsDo_({arg in; in .mark_(mark)}) ;out = this .output ;overridden = out .determinedBy ;(overridden == nil) .ifFalse_({overridden .markUnsatisfied}) ;out .determinedBy_(this) ;(planner .addPropagateMark_(this,mark)) .ifFalse_({this .error_("Cycle encountered adding:\tConstraint removed.") ;^nil}) ;out .mark_(mark)},{overridden = nil ;(strength .sameAs_((Strength .required))) .ifTrue_({this .error_("Failed to satisfy a required constraint")})}) ;^overridden }


}
