Sym  {
var hash;

init_ { arg aHash; hash = aHash }
customHash { ^hash }

* new_ { arg aHash; ^this .new .init_(aHash) }

}
