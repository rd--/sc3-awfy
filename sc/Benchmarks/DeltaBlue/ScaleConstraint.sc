ScaleConstraint  : BinaryConstraint {
var scale,offset;

initializeSrcScaleOffsetDstStrengthAddTo_ { arg srcVar,scaleVar,offsetVar,dstVar,strengthSymbol,planner; super .initializeVarVarStrengthAddTo_(srcVar,dstVar,strengthSymbol,planner) ;scale = scaleVar ;offset = offsetVar ;this .addConstraint_(planner) }
addToGraph { v1 .addConstraint_(this) ;v2 .addConstraint_(this) ;scale .addConstraint_(this) ;offset .addConstraint_(this) ;direction = nil }
removeFromGraph { ( v1 == nil ) .ifFalse_({v1 .removeConstraint_(this)}) ;( v2 == nil ) .ifFalse_({v2 .removeConstraint_(this)}) ;( scale == nil ) .ifFalse_({scale .removeConstraint_(this)}) ;( offset == nil ) .ifFalse_({offset .removeConstraint_(this)}) ;direction = nil }
execute { ( direction == \forward ) .ifTrueIfFalse_({v2 .value_((v1 .value * scale .value) + offset .value)},{v1 .value_((v2 .value - offset .value) .div(scale .value))}) }
inputsDo_ { arg aBlock; ( direction == \forward ) .ifTrueIfFalse_({aBlock .value_(v1) ;aBlock .value_(scale) ;aBlock .value_(offset)},{aBlock .value_(v2) ;aBlock .value_(scale) ;aBlock .value_(offset)}) }
recalculate { var in,out; ( direction == \forward ) .ifTrueIfFalse_({in = v1 ;out = v2},{out = v1 ;in = v2}) ;out .walkStrength_((strength .weakest_(in .walkStrength))) ;out .stay_((in .stay .and_({scale .stay .and_({offset .stay})}))) ;out .stay .ifTrue_({this .execute}) }

* varVarVarVarStrengthAddTo_ { arg src,scale,offset,dst,strengthSymbol,planner; ^this .new .initializeSrcScaleOffsetDstStrengthAddTo_(src,scale,offset,dst,strengthSymbol,planner) }

}
