BinaryConstraint  : AbstractConstraint {
var v1,v2,direction;

initializeVarVarStrengthAddTo_ { arg variable1,variable2,strengthSymbol,planner; super .initialize_(strengthSymbol) ;v1 = variable1 ;v2 = variable2 ;direction = nil }
isSatisfied { ^direction .notNil }
addToGraph { v1 .addConstraint_(this) ;v2 .addConstraint_(this) ;direction = nil }
removeFromGraph { (v1 == nil) .ifFalse_({v1 .removeConstraint_(this)}) ;(v2 == nil) .ifFalse_({v2 .removeConstraint_(this)}) ;direction = nil }
chooseMethod_ { arg mark; (v1 .mark == mark) .ifTrue_({((v2 .mark != mark) .and_({strength .stronger_(v2 .walkStrength)})) .ifTrueIfFalse_({^direction = \forward},{^direction = nil})}) ;(v2 .mark == mark) .ifTrue_({((v1 .mark != mark) .and_({strength .stronger_(v1 .walkStrength)})) .ifTrueIfFalse_({^direction = \backward},{^direction = nil})}) ;(v1 .walkStrength .weaker_(v2 .walkStrength)) .ifTrueIfFalse_({(strength .stronger_(v1 .walkStrength)) .ifTrueIfFalse_({^direction = \backward},{^direction = nil})},{(strength .stronger_(v2 .walkStrength)) .ifTrueIfFalse_({^direction = \forward},{^direction = nil})}) }
execute { this .subclassResponsibility }
inputsDo_ { arg aBlock; ( direction == \forward ) .ifTrueIfFalse_({aBlock .value_(v1)},{aBlock .value_(v2)}) }
inputsHasOne_ { arg aBlock; ^( direction == \forward ) .ifTrueIfFalse_({aBlock .value_(v1)},{aBlock .value_(v2)}) }
markUnsatisfied { direction = nil }
output { (direction == \forward) .ifTrueIfFalse_({^v2},{^v1}) }
recalculate { var in,out; (direction == \forward) .ifTrueIfFalse_({in = v1 ;out = v2},{in = v2 ;out = v1}) ;out .walkStrength_((strength .weakest_(in .walkStrength))) ;out .stay_(in .stay) ;out .stay .ifTrue_({this .execute}) }


}
