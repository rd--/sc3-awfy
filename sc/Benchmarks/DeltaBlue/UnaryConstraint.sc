UnaryConstraint  : AbstractConstraint {
var output,satisfied;

initializeVarStrengthAddTo_ { arg aVariable,strengthSymbol,planner; super .initialize_(strengthSymbol) ;output = aVariable ;satisfied = false ;this .addConstraint_(planner) }
isSatisfied { ^satisfied }
addToGraph { output .addConstraint_(this) ;satisfied = false }
removeFromGraph { ( output == nil ) .ifFalse_({output .removeConstraint_(this)}) ;satisfied = false }
chooseMethod_ { arg mark; satisfied = ( output .mark != mark ) .and_({strength .stronger_(output .walkStrength)}) ;^nil }
execute { this .subclassResponsibility }
inputsDo_ { arg aBlock; }
inputsHasOne_ { arg aBlock; ^false }
markUnsatisfied { satisfied = false }
output { ^output }
recalculate { output .walkStrength_(strength) ;output .stay_(this .isInput .not) ;output .stay .ifTrue_({this .execute}) }


}
