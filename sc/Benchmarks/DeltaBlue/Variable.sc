Variable  {
var value,constraints,determinedBy,walkStrength,stay,mark;

initialize { value = 0 ;constraints = Vector .new_(2) ;determinedBy = nil ;walkStrength = Strength .absoluteWeakest ;stay = true ;mark = 0 }
addConstraint_ { arg aConstraint; constraints .append_(aConstraint) }
constraints { ^constraints }
determinedBy { ^determinedBy }
determinedBy_ { arg aConstraint; determinedBy = aConstraint }
mark { ^mark }
mark_ { arg markValue; mark = markValue }
removeConstraint_ { arg c; constraints .remove_(c) ;( determinedBy == c ) .ifTrue_({determinedBy = nil}) }
stay { ^stay }
stay_ { arg aBoolean; stay = aBoolean }
value { ^value }
value_ { arg anObject; value = anObject }
walkStrength { ^walkStrength }
walkStrength_ { arg aStrength; walkStrength = aStrength }

* new { ^super .new .initialize }
* value_ { arg aValue; var o; o = this .new ;o .value_(aValue) ;^o }

}
