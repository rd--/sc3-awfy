EqualityConstraint  : BinaryConstraint {


initializeVarVarStrengthAddTo_ { arg variable1,variable2,strengthSymbol,planner; super .initializeVarVarStrengthAddTo_(variable1,variable2,strengthSymbol,planner) ;this .addConstraint_(planner) }
execute { ( direction == \forward ) .ifTrueIfFalse_({v2 .value_(v1 .value)},{v1 .value_(v2 .value)}) }

* varVarStrengthAddTo_ { arg variable1,variable2,strengthSymbol,planner; ^this .new .initializeVarVarStrengthAddTo_(variable1,variable2,strengthSymbol,planner) }

}
