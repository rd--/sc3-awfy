Strength  {
var symbolicValue,arithmeticValue;
classvar absoluteStrongest,absoluteWeakest,required,strengthConstants,strengthTable,symAbsoluteStrongest,symRequired,symStrongPreferred,symPreferred,symStrongDefault,symDefault,symWeakDefault,symAbsoluteWeakest;
initializeWith_ { arg symVal; symbolicValue = symVal ;arithmeticValue = Strength .strengthTable .at_(symVal) }
sameAs_ { arg aStrength; ^arithmeticValue == aStrength .arithmeticValue }
stronger_ { arg aStrength; ^arithmeticValue < aStrength .arithmeticValue }
weaker_ { arg aStrength; ^arithmeticValue > aStrength .arithmeticValue }
strongest_ { arg aStrength; (aStrength .stronger_(this)) .ifTrueIfFalse_({^aStrength},{^this}) }
weakest_ { arg aStrength; (aStrength .weaker_(this)) .ifTrueIfFalse_({^aStrength},{^this}) }
arithmeticValue { ^arithmeticValue }

* new_ { arg symVal; ^this .new .initializeWith_(symVal) }
* strengthTable { ^strengthTable }
* createStrengthTable { var table; table = SomIdentityDictionary .new ;table .atPut_(symAbsoluteStrongest,-10000) ;table .atPut_(symRequired,-800) ;table .atPut_(symStrongPreferred,-600) ;table .atPut_(symPreferred,-400) ;table .atPut_(symStrongDefault,-200) ;table .atPut_(symDefault,0) ;table .atPut_(symWeakDefault,500) ;table .atPut_(symAbsoluteWeakest,10000) ;^table }
* createStrengthConstants { var constants; constants = SomIdentityDictionary .new ;strengthTable .keys .forEach_({arg strengthSymbol; constants .atPut_(strengthSymbol,(this .new_(strengthSymbol)))}) ;^constants }
* initialize { symAbsoluteStrongest = Sym .new_(0) ;symRequired = Sym .new_(1) ;symStrongPreferred = Sym .new_(2) ;symPreferred = Sym .new_(3) ;symStrongDefault = Sym .new_(4) ;symDefault = Sym .new_(5) ;symWeakDefault = Sym .new_(6) ;symAbsoluteWeakest = Sym .new_(7) ;strengthTable = this .createStrengthTable ;strengthConstants = this .createStrengthConstants ;absoluteStrongest = Strength .of_(symAbsoluteStrongest) ;absoluteWeakest = Strength .of_(symAbsoluteWeakest) ;required = Strength .of_(symRequired) }
* of_ { arg aSymbol; ^strengthConstants .at_(aSymbol) }
* absoluteStrongest { ^absoluteStrongest }
* absoluteWeakest { ^absoluteWeakest }
* required { ^required }
* symAbsoluteStrongest { ^symAbsoluteStrongest }
* symRequired { ^symRequired }
* symStrongPreferred { ^symStrongPreferred }
* symPreferred { ^symPreferred }
* symStrongDefault { ^symStrongDefault }
* symDefault { ^symDefault }
* symWeakDefault { ^symWeakDefault }
* symAbsoluteWeakest { ^symAbsoluteWeakest }

}
