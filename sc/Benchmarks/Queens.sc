Queens  : Benchmark {
var freeMaxs,freeRows,freeMins,queenRows;

benchmark { var result; result = true ;1 .toDo_(10,{arg i; result = result .and_(this .queens)}) ;^result }
verifyResult_ { arg result; ^result }
queens { freeRows = Array .newWithAll_(8,true) ;freeMaxs = Array .newWithAll_(16,true) ;freeMins = Array .newWithAll_(16,true) ;queenRows = Array .newWithAll_(8,-1) ;^this .placeQueen_(1) }
placeQueen_ { arg c; 1 .toDo_(8,{arg r; (this .rowColumn_(r,c)) .ifTrue_({queenRows .atPut_(r,c) ;this .rowColumnPut_(r,c,false) ;(c == 8) .ifTrue_({^true}) ;(this .placeQueen_(c + 1)) .ifTrue_({^true}) ;this .rowColumnPut_(r,c,true)})}) ;^false }
rowColumn_ { arg r,c; ^(freeRows .at_(r)) && (freeMaxs .at_(c + r)) && (freeMins .at_(c - r + 8)) }
rowColumnPut_ { arg r,c,v; freeRows .atPut_(r,v) ;freeMaxs .atPut_(c + r,v) ;freeMins .atPut_(c - r + 8,v) }


}
