Body  {
var x,y,z,vx,vy,vz,mass;
classvar solarMass;
initXYZVxVyVzMass_ { arg anX,aY,aZ,aVX,aVY,aVZ,aMass; x = anX ;y = aY ;z = aZ ;vx = aVX * Body .daysPerYear ;vy = aVY * Body .daysPerYear ;vz = aVZ * Body .daysPerYear ;mass = aMass * Body .solarMass }
x { ^x }
y { ^y }
z { ^z }
vx { ^vx }
vy { ^vy }
vz { ^vz }
mass { ^mass }
x_ { arg val; x = val }
y_ { arg val; y = val }
z_ { arg val; z = val }
vx_ { arg val; vx = val }
vy_ { arg val; vy = val }
vz_ { arg val; vz = val }
mass_ { arg val; mass = val }
offsetMomentumXYZ_ { arg px,py,pz; vx = 0.0 - (px / Body .solarMass) ;vy = 0.0 - (py / Body .solarMass) ;vz = 0.0 - (pz / Body .solarMass) }
print { "x:  " .print ;x .println ;"y:  " .print ;y .println ;"z:  " .print ;z .println ;"vx: " .print ;vx .println ;"vy: " .print ;vy .println ;"vz: " .print ;vz .println ;"mass: " .print ;mass .println }

* solarMass { ^solarMass }
* daysPerYear { ^365.24 }
* initialize { solarMass = 4 * pi * pi }
* jupiter { ^super .new .initXYZVxVyVzMass_(4.841431442464721,-1.1603200440274284,-0.10362204447112311,0.001660076642744037,0.007699011184197404,-0.0000690460016972063,0.0009547919384243266) }
* saturn { ^super .new .initXYZVxVyVzMass_(8.34336671824458,4.124798564124305,-0.4035234171143214,-0.002767425107268624,0.004998528012349172,0.000023041729757376393,0.0002858859806661308) }
* uranus { ^super .new .initXYZVxVyVzMass_(12.894369562139131,-15.111151401698631,-0.22330757889265573,0.002964601375647616,0.0023784717395948095,-0.000029658956854023756,0.00004366244043351563) }
* neptune { ^super .new .initXYZVxVyVzMass_(15.379697114850917,-25.919314609987964,0.17925877295037118,0.0026806777249038932,0.001628241700382423,-0.00009515922545197159,0.000051513890204661145) }
* sun { ^super .new .initXYZVxVyVzMass_(0.0,0.0,0.0,0.0,0.0,0.0,1.0) }

}
