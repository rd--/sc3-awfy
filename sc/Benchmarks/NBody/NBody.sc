NBody  : Benchmark {


innerBenchmarkLoop_ { arg innerIterations; var system; system = NBodySystem .new ;1 .toDo_(innerIterations,{arg i; system .advance_(0.01)}) ;^this .verifyFor_(system .energy,innerIterations) }
verifyFor_ { arg result,innerIterations; ( innerIterations == 250000 ) .ifTrue_({^result == -0.1690859889909308}) ;( innerIterations == 1 ) .ifTrue_({^result == -0.16907495402506745}) ;("No verification result for " ++ innerIterations .asString ++ " found") .println ;("Result is: " ++ result .asString) .println ;^false }

* new { Body .initialize ;^super .new }

}
