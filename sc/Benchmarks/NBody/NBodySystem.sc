NBodySystem  {
var bodies;

initialize { bodies = this .createBodies }
createBodies { var px,py,pz,bodies; bodies = Array .new_(5) ;bodies .atPut_(1,Body .sun) ;bodies .atPut_(2,Body .jupiter) ;bodies .atPut_(3,Body .saturn) ;bodies .atPut_(4,Body .uranus) ;bodies .atPut_(5,Body .neptune) ; px = 0.0 ;py = 0.0 ;pz = 0.0 ;bodies .do_({arg b; px = px + (b .vx * b .mass) ;py = py + (b .vy * b .mass) ;pz = pz + (b .vz * b .mass)}) ;(bodies .at_(1)) .offsetMomentumXYZ_(px,py,pz) ;^bodies }
advance_ { arg dt; 1 .toDo_(bodies .size,{arg i; var iBody; iBody = bodies .at_(i) ;( i + 1 ) .toDo_(bodies .size,{arg j; var dx,dy,dz,jBody,dSquared,distance,mag; jBody = bodies .at_(j) ;dx = iBody .x - jBody .x ;dy = iBody .y - jBody .y ;dz = iBody .z - jBody .z ;dSquared = (dx * dx) + (dy * dy) + (dz * dz) ;distance = dSquared .sqrt ;mag = dt / (dSquared * distance) ;iBody .vx_(iBody .vx - (dx * jBody .mass * mag)) ;iBody .vy_(iBody .vy - (dy * jBody .mass * mag)) ;iBody .vz_(iBody .vz - (dz * jBody .mass * mag)) ;jBody .vx_(jBody .vx + (dx * iBody .mass * mag)) ;jBody .vy_(jBody .vy + (dy * iBody .mass * mag)) ;jBody .vz_(jBody .vz + (dz * iBody .mass * mag))})}) ;bodies .do_({arg body; body .x_(body .x + (dt * body .vx)) ;body .y_(body .y + (dt * body .vy)) ;body .z_(body .z + (dt * body .vz))}) }
energy { var dx,dy,dz,distance,e; e = 0.0 ;1 .toDo_(bodies .size,{arg i; var iBody; iBody = bodies .at_(i) ;e = e + (0.5 * iBody .mass * ((iBody .vx * iBody .vx) + (iBody .vy * iBody .vy) + (iBody .vz * iBody .vz))) ;( i + 1 ) .toDo_(bodies .size,{arg j; var jBody; jBody = bodies .at_(j) ;dx = iBody .x - jBody .x ;dy = iBody .y - jBody .y ;dz = iBody .z - jBody .z ;distance = ((dx * dx) + (dy * dy) + (dz * dz)) .sqrt ;e = e - ((iBody .mass * jBody .mass) / distance)})}) ;^e }

* new { ^super .new .initialize }

}
