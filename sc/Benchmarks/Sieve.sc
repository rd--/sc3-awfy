Sieve  : Benchmark {


benchmark { var flags; flags = Array .newWithAll_(5000,true) ;^this .sieveSize_(flags,5000) }
verifyResult_ { arg result; ^669 == result }
sieveSize_ { arg flags,size; var primeCount; primeCount = 0 ;2 .toDo_(size,{arg i; (flags .at_(i - 1)) .ifTrue_({var k; primeCount = primeCount + 1 ;k = i + i ;{k <= size} .whileTrue_({flags .atPut_(k - 1,false) ;k = k + i})})}) ;^primeCount }


}
