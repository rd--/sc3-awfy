Benchmark  {


innerBenchmarkLoop_ { arg innerIterations; 1 .toDo_(innerIterations,{arg i; (this .verifyResult_(this .benchmark)) .ifFalse_({^false})}) ;^true }
benchmark { this .subclassResponsibility }
verifyResult_ { arg result; this .subclassResponsibility }


}
