Vector2D  {
var x,y;

x { ^x }
y { ^y }
initXY_ { arg anX,aY; x = anX ;y = aY }
plus_ { arg other; ^Vector2D .xY_(x + other .x,y + other .y) }
minus_ { arg other; ^Vector2D .xY_(x - other .x,y - other .y) }
compareTo_ { arg other; var result; result = this .compareAnd_(x,other .x) ;( result != 0 ) .ifTrue_({^result}) ;^this .compareAnd_(y,other .y) }
compareAnd_ { arg a,b; ( a == b ) .ifTrue_({^0}) ;( a < b ) .ifTrue_({^-1}) ;( a > b ) .ifTrue_({^1}) ;( a == a ) .ifTrue_({^1}) ;^-1 }

* xY_ { arg anX,aY; ^this .new .initXY_(anX,aY) }

}
