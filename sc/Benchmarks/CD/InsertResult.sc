InsertResult  {
var isNewEntry,newNode,oldValue;

initNodeValue_ { arg aBool,aNode,val; isNewEntry = aBool ;newNode = aNode ;oldValue = val }
isNewEntry { ^isNewEntry }
newNode { ^newNode }
oldValue { ^oldValue }

* newNodeValue_ { arg isNewEntry,newNode,oldValue; ^this .new .initNodeValue_(isNewEntry,newNode,oldValue) }

}
