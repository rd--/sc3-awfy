CDNode  {
var key,value,left,right,parent,color;

initValue_ { arg aKey,aValue; key = aKey ;value = aValue ;color = \red }
key { ^key }
value { ^value }
value_ { arg val; value = val }
left { ^left }
left_ { arg n; left = n }
right { ^right }
right_ { arg n; right = n }
parent { ^parent }
parent_ { arg n; parent = n }
color { ^color }
color_ { arg sym; color = sym }
successor { var x,y; x = this ;x .right .notNil .ifTrue_({^RedBlackTree .treeMinimum_(x .right)}) ;y = x .parent ;{y .notNil .and_({x == y .right})} .whileTrue_({x = y ;y = y .parent}) ;^y }

* keyValue_ { arg key,value; ^this .new .initValue_(key,value) }

}
