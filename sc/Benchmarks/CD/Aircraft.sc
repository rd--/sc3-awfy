Aircraft  {
var callsign,position;

initPos_ { arg aCallsign,aPosition; callsign = aCallsign ;position = aPosition }
callsign { ^callsign }
position { ^position }

* newPos_ { arg callsign,position; ^this .new .initPos_(callsign,position) }

}
