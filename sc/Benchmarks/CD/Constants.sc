Constants  {

classvar horizontal,vertical;

* initialize { horizontal = Vector2D .xY_(this .goodVoxelSize,0.0) ;vertical = Vector2D .xY_(0.0,this .goodVoxelSize) }
* minX { ^0.0 }
* minY { ^0.0 }
* maxX { ^1000.0 }
* maxY { ^1000.0 }
* minZ { ^0.0 }
* maxZ { ^10.0 }
* proximityRadius { ^1.0 }
* goodVoxelSize { ^2.0 }
* horizontal { ^horizontal }
* vertical { ^vertical }

}
