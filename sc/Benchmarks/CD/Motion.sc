Motion  {
var callsign,posOne,posTwo;

initOldNew_ { arg aCallsign,aPosOne,aPosTwo; callsign = aCallsign ;posOne = aPosOne ;posTwo = aPosTwo }
callsign { ^callsign }
posOne { ^posOne }
posTwo { ^posTwo }
delta { ^posTwo .minus_(posOne) }
findIntersection_ { arg other; var init1,init2,vec1,vec2,radius,a,dist; init1 = posOne ;init2 = other .posOne ;vec1 = this .delta ;vec2 = other .delta ;radius = Constants .proximityRadius ;a = (vec2 .minus_(vec1)) .squaredMagnitude ;( a != 0.0 ) .ifTrue_({var b,c,discr,v1,v2; b = 2.0 * ((init1 .minus_(init2)) .dot_((vec1 .minus_(vec2)))) ;c = ((0.0 - radius) * radius) + ((init2 .minus_(init1)) .squaredMagnitude) ;discr = (b * b) - (4.0 * a * c) ;( discr < 0.0 ) .ifTrue_({^nil}) ;v1 = ((0.0 - b) - discr .sqrt) / (2.0 * a) ;v2 = ((0.0 - b) + discr .sqrt) / (2.0 * a) ;(( v1 <= v2 ) .and_({((( v1 <= 1.0 ) .and_({1.0 <= v2})) .or_({(( v1 <= 0.0 ) .and_({0.0 <= v2})) .or_({(( 0.0 <= v1 ) .and_({v2 <= 1.0}))})}))})) .ifTrue_({var v,result1,result2,result; ( v1 <= 0.0 ) .ifTrueIfFalse_({v = 0.0},{v = v1}) ;result1 = init1 .plus_((vec1 .times_(v))) ;result2 = init2 .plus_((vec2 .times_(v))) ;result = (result1 .plus_(result2)) .times_(0.5) ;(( result .x >= Constants .minX ) .and_({( result .x <= Constants .maxX ) .and_({( result .y >= Constants .minY ) .and_({( result .y <= Constants .maxY ) .and_({( result .z >= Constants .minZ ) .and_({result .z <= Constants .maxZ})})})})})) .ifTrue_({^result})}) ;^nil}) ;dist = (init2 .minus_(init1)) .magnitude ;( dist <= radius ) .ifTrue_({^(init1 .plus_(init2)) .times_(0.5)}) ;^nil }

* newOldNew_ { arg callsign,posOne,posTwo; ^this .new .initOldNew_(callsign,posOne,posTwo) }

}
