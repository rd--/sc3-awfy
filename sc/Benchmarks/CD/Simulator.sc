Simulator  {
var aircrafts;

init_ { arg numAircrafts; aircrafts = Vector .new ;0 .toDo_(numAircrafts - 1,{arg i; aircrafts .append_((CallSign .new_(i)))}) }
simulate_ { arg time; var frame; frame = Vector .new ;0 .toByDo_(aircrafts .size - 2,2,{arg i; frame .append_((Aircraft .newPos_((aircrafts .at_(i + 1)),(Vector3D .xYZ_(time,(time .cos * 2.0) + (i * 3.0),10.0))))) ;frame .append_((Aircraft .newPos_((aircrafts .at_(i + 2)),(Vector3D .xYZ_(time,(time .sin * 2.0) + (i * 3.0),10.0)))))}) ;^frame }

* new_ { arg numAircrafts; ^this .new .init_(numAircrafts) }

}
