RbtEntry  {
var key,value;

initValue_ { arg aKey,val; key = aKey ;value = val }
key { ^key }
value { ^value }

* keyValue_ { arg key,value; ^this .new .initValue_(key,value) }

}
