Collision  {
var aircraftA,aircraftB,position;

initBPos_ { arg anA,aB,aPos; aircraftA = anA ;aircraftB = aB ;position = aPos }
aircraftA { ^aircraftA }
aircraftB { ^aircraftB }
position { ^position }

* aBPos_ { arg aircraftA,aircraftB,position; ^this .new .initBPos_(aircraftA,aircraftB,position) }

}
