CallSign  {
var value;

init_ { arg val; value = val }
value { ^value }
compareTo_ { arg other; ^( value == other .value ) .ifTrueIfFalse_({0},{( value < other .value ) .ifTrueIfFalse_({-1},{1})}) }

* new_ { arg val; ^this .new .init_(val) }

}
