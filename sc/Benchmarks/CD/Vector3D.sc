Vector3D  {
var x,y,z;

x { ^x }
y { ^y }
z { ^z }
initXYZ_ { arg anX,aY,aZ; x = anX ;y = aY ;z = aZ }
plus_ { arg other; ^Vector3D .xYZ_(x + other .x,y + other .y,z + other .z) }
minus_ { arg other; ^Vector3D .xYZ_(x - other .x,y - other .y,z - other .z) }
dot_ { arg other; ^(x * other .x) + (y * other .y) + (z * other .z) }
squaredMagnitude { ^this .dot_(this) }
magnitude { ^this .squaredMagnitude .sqrt }
times_ { arg amount; ^Vector3D .xYZ_(x * amount,y * amount,z * amount) }

* xYZ_ { arg x,y,z; ^this .new .initXYZ_(x,y,z) }

}
