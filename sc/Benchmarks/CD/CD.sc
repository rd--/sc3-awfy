CD  : Benchmark {


benchmark_ { arg numAircrafts; var numFrames,simulator,detector,actualCollisions; numFrames = 200 ;simulator = Simulator .new_(numAircrafts) ;detector = CollisionDetector .new ;actualCollisions = 0 ;0 .toDo_(numFrames - 1,{arg i; var time,collisions; time = i / 10.0 ;collisions = detector .handleNewFrame_((simulator .simulate_(time))) ;actualCollisions = actualCollisions + collisions .size}) ;^actualCollisions }
innerBenchmarkLoop_ { arg innerIterations; ^this .verifyResultFor_((this .benchmark_(innerIterations)),innerIterations) }
verifyResultFor_ { arg actualCollisions,numAircrafts; ( numAircrafts == 1000 ) .ifTrue_({^actualCollisions == 14484}) ;( numAircrafts == 500 ) .ifTrue_({^actualCollisions == 14484}) ;( numAircrafts == 250 ) .ifTrue_({^actualCollisions == 10830}) ;( numAircrafts == 200 ) .ifTrue_({^actualCollisions == 8655}) ;( numAircrafts == 100 ) .ifTrue_({^actualCollisions == 4305}) ;( numAircrafts == 10 ) .ifTrue_({^actualCollisions == 390}) ;( numAircrafts == 2 ) .ifTrue_({^actualCollisions == 42}) ;ScriptConsole .println_(("No verification result for " ++ numAircrafts .asString ++ " found.")) ;ScriptConsole .println_(("Result is: " ++ actualCollisions .asString)) ;^false }

* new { Constants .initialize ;^super .new }

}
