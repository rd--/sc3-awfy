TowersDisk  : Object {
var size,next;

initialize_ { arg anInt; size = anInt }
size { ^size }
next { ^next }
next_ { arg value; next = value }

* new_ { arg size; ^super .new .initialize_(size) }

}
