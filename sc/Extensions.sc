+ True {
    ifTrue_ {arg trueFunc; ^trueFunc.value}
    ifFalse_ {arg falseFunc; ^this}
    ifTrueIfFalse_ { arg trueFunc, falseFunc; ^trueFunc.value }
	and_ { arg that; ^that.value }
	or_ { arg that; ^this }
}

+ False {
    ifFalse_ {arg falseFunc; ^falseFunc.value}
    ifTrue_ {arg trueFunc; ^this}
    ifTrueIfFalse_ { arg trueFunc, falseFunc; ^falseFunc.value }
	and_ { arg that; ^this }
	or_ { arg that; ^that.value }
}

// SOM & Smalltalk are one indexed
+ Array {
    *new_ { arg k; ^Array.newClear(k) }
    *newWithAll_ { arg k, e; ^Array.fill(k, e) }
    at_ { arg k; ^this.at(k-1) }
    atPut_ { arg k, e; this.put(k-1, e) }
    do_ { arg f; this.do(f) }
    doIndexes_ { arg f; this.size.do { |i| f.value(i + 1) } }
    *withWith_ { arg p,q; ^Array.with(p,q) }
    length { ^this.size }
}

+ Integer {
    // https://github.com/supercollider/supercollider/issues/5520
    toDo_ { arg k, f; (this <= k).ifTrue_({this.for(k, f)}) }
    downToDo_ { arg k, f;
        var i = this;
        { i >= k }.while { f.value(i); i = i - 1 }
	}
    toByDo_ { arg j, k, f; this.forBy(j, k, f) }
    timesRepeat_ { arg aBlock; var remaining = this; {(remaining = remaining - 1) >= 0}.whileTrue_({aBlock.value}) }
}

+ Object {
    error_ { arg x; this.postln; x.error }
    ifNil_ { arg x; ^this }
	<> { arg x; ^this != x }
}

+ SimpleNumber {
	bitAnd_ { |x| _BitAnd }
	bitOr_ { |x| _BitOr }
	bitXor_ { |x| _BitXor }
}

// Smalltalk & SOM are one-indexed.  SOM has no character type, i.e. String>>at: returns a String.
+ String {
    println { this.postln }
    stringAt_ { arg x; ^this.copyRange(x-1,x-1) }
    copyFromTo_ { arg i, j; ^this.copyRange(i-1,j-1) }
}

+ Magnitude {
    max_ { arg x; ^this.max(x) }
}

+ Function {
    whileTrue_ { arg f; this.while(f) }
    whileFalse_ { arg f; {this.value.not}.while(f) }
    value_ { arg x; ^this.value(x) }
    valueValue_ { arg p, q; ^this.value(p,q) }
}

+ Float {
    *infinity { ^inf }
    *negativeInfinity { ^-inf }
}

+ Nil {
    ifNil_ { arg x; ^x.value }
}
