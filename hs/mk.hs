import System.FilePath {- filepath -}

import qualified Language.Smalltalk.Ansi as St {- stsc3 -}
import qualified Language.Smalltalk.Ansi.Print.Stc as Stc {- stsc3 -}
import qualified Language.Smalltalk.Som as Som {- stsc3 -}

som_to_sc :: Bool -> String -> String
som_to_sc spl txt =
  let som = St.stParse Som.classDefinition txt
  in Stc.sc_ClassDefinition_pp spl som

som_to_sc_io :: Bool -> FilePath -> FilePath -> IO ()
som_to_sc_io spl som_fn sc_fn = do
  txt <- readFile som_fn
  writeFile sc_fn (som_to_sc spl txt)

somBenchmarksDirectory :: FilePath
somBenchmarksDirectory = "/home/rohan/opt/src/smarr/are-we-fast-yet/benchmarks/SOM"

somBenchmarksFilename :: String -> FilePath
somBenchmarksFilename nm = somBenchmarksDirectory </> nm <.> "som"

scBenchmarksFilename :: Bool -> String -> FilePath
scBenchmarksFilename spl nm =
  if spl
  then "/home/rohan/sw/sc3-awfy/sl/Benchmarks" </> nm <.> "sl"
  else "/home/rohan/sw/sc3-awfy/sc/Benchmarks" </> nm <.> "sc"

{-
> translateBenchmark True "TowersDisk"
> mapM_ (\x -> putStrLn x >> translateBenchmark True x) benchmarkClasses
-}
translateBenchmark :: Bool -> String -> IO ()
translateBenchmark spl nm = som_to_sc_io spl (somBenchmarksFilename nm) (scBenchmarksFilename spl nm)

benchmarkClasses :: [String]
benchmarkClasses =
  ["Ball"
  ,"Benchmark"
  ,"Bounce"
  ,"CD/Aircraft"
  ,"CD/CallSign"
  ,"CD/CD"
  ,"CD/CollisionDetector"
  ,"CD/Collision"
  ,"CD/Constants"
  ,"CD/InsertResult"
  ,"CD/Motion"
  ,"CD/Node"
  ,"CD/RbtEntry"
  ,"CD/RedBlackTree"
  ,"CD/Simulator"
  ,"CD/Vector2D"
  ,"CD/Vector3D"
  ,"Core/DictEntry"
  ,"Core/DictIdEntry"
  ,"Core/DuplicateVector"
  ,"Core/SomDictionary"
  ,"Core/SomIdentityDictionary"
  ,"Core/SomIdentitySet"
  ,"Core/SomSet"
  ,"DeltaBlue/AbstractConstraint"
  ,"DeltaBlue/BinaryConstraint"
  ,"DeltaBlue/DeltaBlue"
  ,"DeltaBlue/EditConstraint"
  ,"DeltaBlue/EqualityConstraint"
  ,"DeltaBlue/Planner"
  ,"DeltaBlue/Plan"
  ,"DeltaBlue/ScaleConstraint"
  ,"DeltaBlue/StayConstraint"
  ,"DeltaBlue/Strength"
  ,"DeltaBlue/Sym"
  ,"DeltaBlue/UnaryConstraint"
  ,"DeltaBlue/Variable"
  ,"Havlak/BasicBlockEdge"
  ,"Havlak/BasicBlock"
  ,"Havlak/ControlFlowGraph"
  ,"Havlak/HavlakLoopFinder"
  ,"Havlak/Havlak"
  ,"Havlak/LoopStructureGraph"
  ,"Havlak/LoopTesterApp"
  ,"Havlak/SimpleLoop"
  ,"Havlak/UnionFindNode"
  ,"List"
  ,"ListElement"
  ,"Mandelbrot"
  ,"NBody/Body"
  ,"NBody/NBody"
  ,"NBody/NBodySystem"
  ,"Permute"
  ,"Queens"
  ,"Richards/DeviceTaskDataRecord"
  ,"Richards/HandlerTaskDataRecord"
  ,"Richards/IdleTaskDataRecord"
  ,"Richards/Packet"
  ,"Richards/RBObject"
  ,"Richards/Richards"
  ,"Richards/Scheduler"
  ,"Richards/TaskControlBlock"
  ,"Richards/TaskState"
  ,"Richards/WorkerTaskDataRecord"
  ,"Json/HashIndexTable"
  ,"Json/JsonArray"
  ,"Json/JsonLiteral"
  ,"Json/JsonNumber"
  ,"Json/JsonObject"
  ,"Json/JsonParser"
  ,"Json/Json"
  ,"Json/JsonString"
  ,"Json/JsonValue"
  ,"Json/ParseException"
  ,"Sieve"
  ,"SomRandom"
  ,"Storage"
  ,"Towers"
  ,"TowersDisk"]

-- * TestSuite

somTestSuiteDirectory :: FilePath
somTestSuiteDirectory = "/home/rohan/opt/src/som/SOM/TestSuite"

somTestSuiteFilename :: String -> FilePath
somTestSuiteFilename nm = somTestSuiteDirectory </> nm <.> "som"

scTestSuiteFilename :: Bool -> String -> FilePath
scTestSuiteFilename spl nm =
  if spl
  then "/home/rohan/sw/sc3-awfy/sl/TestSuite" </> nm <.> "sl"
  else "/home/rohan/sw/sc3-awfy/sc/TestSuite" </> nm <.> "sc"

{-
> mapM_ (\x -> putStrLn x >> translateTestSuite x) testSuiteClasses
-}
translateTestSuite :: Bool -> String -> IO ()
translateTestSuite spl nm = som_to_sc_io spl (somTestSuiteFilename nm) (scTestSuiteFilename spl nm)

testSuiteClasses :: [String]
testSuiteClasses =
  ["ArrayTest"
  ,"BlockTest"
  ,"ClassA"
  ,"ClassB"
  ,"ClassC"
  ,"ClassLoadingTest"
  ,"ClassStructureTest"
  ,"ClosureTest"
  ,"CoercionTest"
  ,"CompilerReturnTest"
  ,"DoesNotUnderstandMessage"
  ,"DoesNotUnderstandTest"
  ,"DoubleTest"
  ,"EmptyTest"
  ,"GlobalTest"
  ,"HashTest"
  ,"IntegerTest"
  ,"PreliminaryTest"
  ,"ReflectionTest"
  ,"SelfBlockTest"
  ,"SetTest"
  ,"SpecialSelectorsTest"
  ,"StringTest"
  ,"SuperTest"
  ,"SuperTestSuperClass"
  ,"SymbolTest"
  ,"SystemTest"
  ,"TestCase"
  ,"TestHarness"
  ,"TestRunner"
  ,"VectorTest"]
